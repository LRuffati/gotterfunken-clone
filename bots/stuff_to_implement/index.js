// Discord consts
const Discord = require('discord.js');
const bot = new Discord.Client();
const token = 'NjQyMTIyMzk5MDQ2ODI4MDM1.XcUX6w.1wcKRHUgGc8c-lf2eg_ryvkvz2w';
const fs = require('fs');
let slChannel;
const RichEmbed = Discord.RichEmbed;

// LOCAL VARIABLES

let status = {
	PENDING: 'PENDING',
	VOTED: 'VOTED',
	IMPLEMENTED: 'IMPLEMENTED',
};

let suggestionsList;
try {
	//load from the file into memory
	suggestionsList = JSON.parse(fs.readFileSync('./suggestionsList.json', 'utf8'));
} catch(e) {
	//suggestionsList = [];
	console.log(e);
}

//FUNCTIONS

function ban(member, author){
	// ban
	let name = member.user.username;
	member.ban().then((member) => {
		// Successmessage
		let embed = new RichEmbed()
								.setTitle('Ban by '+ author)
								.setDescription("Banned " + name)
								.setColor(0x003399);
		if(slChannel) slChannel.send(embed);
	}).catch(() => {
		 // Failmessage
		 let embed = new RichEmbed()
								.setTitle('ERROR: attempted Ban by '+ author)
								.setDescription("NOT Banned " + name)
								.setColor(0x003399);
		if(slChannel) slChannel.send(embed);
	});
}

function kick(member, author){
	//kick
	let name = member.user.username;
	member.kick().then((member) => {
		// Successmessage
		let embed = new RichEmbed()
								.setTitle('Kick by '+ author)
								.setDescription("Kicked " + name)
								.setColor(0x003399);
		if(slChannel) slChannel.send(embed);
	}).catch(() => {
		 // Failmessage
		 let embed = new RichEmbed()
								.setTitle('ERROR: attempted Kick by '+ author)
								.setDescription("NOT Kicked " + name)
								.setColor(0x003399);
		if(slChannel) slChannel.send(embed);
	});
}

function remove(member, author, message){
	let embed = new RichEmbed()
								.setTitle('Delete by '+ member)
								.setDescription("Deleted " + author + ": " + message.content)
								.setColor(0x003399);

	let errorEmbed = new RichEmbed()
								.setTitle('ERROR: attempted Delete by '+ member)
								.setDescription("NOT Deleted " + author + ": " + message.content)
								.setColor(0x003399);
	
	message.delete().then(message =>{
		if(slChannel) slChannel.send(embed);

	}).catch(() => {
		if(slChannel) slChannel.send(errorEmbed);
	});
}

// bot online
bot.on('ready', () =>{
	console.log('This bot is online');
	slChannel = bot.channels.find(c=>c.name === 'server-log');
});

// Bot channel/dm message proc
bot.on('message', msg=>{
	if(msg.guild && msg.guild.roles) {

		// only in channels
		if(msg.channel && msg.member && msg.member.user && msg.member.user.username && msg.member.user.username.toLowerCase() !== 'test-bot') {

			// #senate or suggestions channel  
			if(msg.channel.name && msg.channel.name === 'senate' || msg.channel.name && msg.channel.name === 'suggestions') {

				//list of the avaiable suggestions related commands
				if(msg.content.toLowerCase().startsWith('!suggestionscommands')){
					msg.author.send('**These are the current suggestions related commands**'+'\n'+
					'\`\`\`' +
					'!suggestionsCommands (lists all the available commands)'+'\n'+'\n'+
					'!addSuggestion (adds a suggestion to the list)'+'\n'+'\n'+
					'!listSuggestions (lists all the suggestions)'+'\n'+'\n'+
					'!listPendingSuggestions (lists PENDING suggestions)'+'\n'+'\n'+
					'!listVotedSuggestions (lists VOTED suggestions)'+'\n'+'\n'+
					'!listImplementedSuggestions (lists IMPLEMENTED suggestions)'+'\n'+'\n'+
					'**MODS and TRIBUNES ONLY**: !voteSuggestion (example: !voteSuggestion code 2)'+'\n'+'\n'+
					'**MODS and TRIBUNES ONLY**: !implementSuggestion (example: !implementSggestion code 0)'+'\n'+'\n'+
					'**MODS and TRIBUNES ONLY**: !cleanSuggestions (removes all implemented suggestions from the list)'+
					'\`\`\`');

					msg.delete();
				};

				// addSuggestion
				if(msg.content.toLowerCase().startsWith('!addsuggestion')) {

					let c;
					if (suggestionsList.length !== 0){
						c = suggestionsList[(suggestionsList.length-1)].code;
						c++;
					}
					else {
						c = 0;
					}
					let suggestion = {
						code : c,
						description : msg.content.slice(15),
						state : 'PENDING'
					};

					suggestionsList.push(suggestion);

					//write the list into the file
					fs.writeFileSync('./suggestionsList.json', JSON.stringify(suggestionsList));

					msg.channel.send('Suggestion added to the list with the code: '+ suggestion.code);
				}

				// listSuggestions
				if(msg.content.toLowerCase().startsWith('!listsuggestions')) {

					if(suggestionsList.length>0){
						let DM = "";
						DM +='*Suggestions list* \n';
						suggestionsList.forEach(element => {
							DM +='\`\`\`'+element.code+' - '+element.description+' - '+element.state+'\`\`\`'
							;
						});
						msg.author.send(DM);
					}
					else {
						msg.author.send('**Empty list**');
					}

					msg.delete();
				}

				// listPendingSuggestions
				if(msg.content.toLowerCase().startsWith('!listPendingsuggestions')) {

					let flag = false;
					let DM = '*PENDING suggestions list*';
					suggestionsList.forEach(element => {
						if(element.state === status.PENDING){
							flag = true;
							DM += 
								'\`\`\`'+element.code+' - '+element.description+' - '+element.state+'\`\`\`'
							;
						}
					});
					if(flag === true){
						msg.author.send(DM);
					}
					if (flag === false) {
						msg.author.send('**Empty PENDING list**');
					}

					msg.delete();
				}

				// listVotedSuggestions
				if(msg.content.toLowerCase().startsWith('!listvotedsuggestions')) {

					let flag = false;
					let DM ='*VOTED suggestions list*';
					suggestionsList.forEach(element => {
						if(element.state === status.VOTED){
							flag = true;
							DM += 
								'\`\`\`'+element.code+' - '+element.description+' - '+element.state+'\`\`\`'
							;
						}
					});
					if(flag === true){
						msg.author.send(DM);
					}
					if (flag === false) {
						msg.author.send('**Empty VOTED list**');
					}

					msg.delete();
				}

				// listImplementedSuggestions
				if(msg.content.toLowerCase().startsWith('!listimplementedsuggestions')) {

					let flag = false;
					DM ='*IMPLEMENTED suggestions list*';
					suggestionsList.forEach(element => {
						if(element.state === status.IMPLEMENTED){
							flag = true;
							DM +=
								'\`\`\`'+element.code+' - '+element.description+' - '+element.state+'\`\`\`'
							;
						}
					});
					if(flag === true){
						msg.author.send(DM);
					}
					if (flag === false) {
						msg.author.send('**Empty IMPLEMENTED list**');
					}

					msg.delete();
				}
			}

			// #senate channel 
			if(msg.channel.name && msg.channel.name === 'senate') {

				// voteSuggestion
				if(msg.content.toLowerCase().startsWith('!votesuggestion')) {
					let r = /\d+/;
					let id = msg.content.match(r)[0];
					let flag = false;
					if (id !== null){
						suggestionsList.forEach(element => {
							if(id == element.code && element.state === status.PENDING){
								console.log('vote id found');
								msg.channel.send(
									'**VOTING**'+'\`\`\`'+element.description+'\`\`\`'
								).then(function (message) {
									message.react("👍");
									message.react("👎");
									message.react("🟡");
								})
								element.state = status.VOTED;
								flag = true;
								fs.writeFileSync('./suggestionsList.json', JSON.stringify(suggestionsList));
								
							}
						})		
					
					}
					if (flag === false) {
						msg.channel.send('Suggestion not found or already voted');
					}
				}

				// Set suggestion to implemented
				if(msg.content.toLowerCase().startsWith('!implementsuggestion')) {
					let r = /\d+/;
					let id = msg.content.match(r)[0];
					let flag = false;
					if (id !== null){
						suggestionsList.forEach(element => {
							if(id == element.code && element.state === status.VOTED){
								console.log('suggestion id found');
								element.state = status.IMPLEMENTED;
								fs.writeFileSync('./suggestionsList.json', JSON.stringify(suggestionsList));
								flag = true;
								msg.channel.send('Suggestion '+'\`'+'['+element.code+' - '+element.description+' - '+element.state+']'+'\`'+' set as implemented');
							}
						})		
					
					}
					if (flag === false) {
						msg.channel.send('Suggestion not found or already implemented');
					}
				}

				// cleanSuggestions
				if(msg.content.toLowerCase().startsWith('!cleansuggestions')) {
					let flag = false;
					for( let i = 0; i < suggestionsList.length; i++){ 
						if ( suggestionsList[i].state === status.IMPLEMENTED) {
						  suggestionsList.splice(i, 1); 
						  i--;
						  flag = true;
						}
					 }
					 if (flag === true){
						fs.writeFileSync('./suggestionsList.json', JSON.stringify(suggestionsList));
						msg.channel.send('All implemented suggestions have been removed');
					 }
					 else {
						msg.channel.send('No implemented suggestions to remove');
					 }
				}
			}

			/*
			//Mods only commands
			if(msg.member && msg.member.roles && msg.member.roles.find(r=>r.name === 'Moderator')){

			}
			*/

			//Mods+Tribunes commands
			if(msg.member && msg.member.roles && msg.member.roles.find(r=>r.name === 'Moderator') || msg.member.roles.find(r=>r.name === 'Tribune')){
				// !ban
				if(msg.content.toLowerCase().startsWith('!ban')){
					let member = msg.mentions.members.first();
					let author = msg.member.user.username;
					
					if(msg.member.roles.find(r=>r.name === 'Moderator')){
						ban(member,author);
					}

					if(msg.member.roles.find(r=>r.name === 'Tribune') && !member.roles.find(r=>r.name === 'Tribune')){
						ban(member,author);
					}
				}
			}

			//Mods+Tribunes+Wardens commands
			if(msg.member && msg.member.roles && msg.member.roles.find(r=>r.name === 'Moderator') || msg.member.roles.find(r=>r.name === 'Tribune') || msg.member.roles.find(r=>r.name === 'Warden')){
				// !kick
				if(msg.content.toLowerCase().startsWith('!kick')){
					let member = msg.mentions.members.first();
					let author = msg.member.user.username;
					
					if(msg.member.roles.find(r=>r.name === 'Moderator')){
						console.log("mod trigger");
						kick(member,author);
					}

					if(msg.member.roles.find(r=>r.name === 'Tribune') && !member.roles.find(r=>r.name === 'Tribune')){
						console.log("tribune trigger");
						kick(member,author);
					}

					if(msg.member.roles.find(r=>r.name === 'Warden') && !member.roles.find(r=>r.name === 'Tribune') && !member.roles.find(r=>r.name === 'Warden')){
						console.log("warden trigger");
						kick(member,author);
					}
				}
			}
		}
	}
});

//Bot reaction proc
bot.on("messageReactionAdd", (messageReaction, user) => {
	if (messageReaction.emoji.name === '❌'){

		let mods = messageReaction.message.guild.members.filter(member => { 
			return member.roles.find(r => r.name === 'Moderator');
		}).map(member => {
			return member.user.id;
		})

		let tribunes = messageReaction.message.guild.members.filter(member => { 
			return member.roles.find(r => r.name === 'Tribune');
		}).map(member => {
			return member.user.id;
		})

		let wardens = messageReaction.message.guild.members.filter(member => { 
			return member.roles.find(r => r.name === 'Warden');
		}).map(member => {
			return member.user.id;
		})

		if(mods.includes(user.id) || tribunes.includes(user.id)){
			let member = user.username;
			let author = messageReaction.message.author.username;
			let message = messageReaction.message;

			remove(member, author, message);
		}

		if(wardens.includes(user.id) && messageReaction.message.channel.name && messageReaction.message.channel.name === "debates"){
			let member = user.username;
			let author = messageReaction.message.author.username;
			let message = messageReaction.message;

			remove(member, author, message);
		}
	}
});

bot.login(token);