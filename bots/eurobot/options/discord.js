module.exports = {

	// discord bot token
	token:process.env.EUROBOT,

	intervals:{
		news:'*/1 * * * *',
		calendar:'0 0 0 * * *'
	},
	guild:'Forum Götterfunken',

	channels:{
		'serverLog':'server-log',
	}

};
