# Eurobot
Forum Gotterfunken Discord Bot

## Setup

Node Modules:

    npm i


Exports in \~\\.bashrc:

	export EUROBOT=""

## Run

    npm start

## Commands

    !lang <language name>
    !language <language name>

Change language role.

    !country <country name>

Change country role.